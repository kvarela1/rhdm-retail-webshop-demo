Retail Webshop Install Demo 
===========================
Project to automate the installation of Red Hat Decision Manager with the Coolstore project and Vaadin front end web shop.


Install on your machine
-----------------------
1. [Download and unzip.](https://gitlab.com/bpmworkshop/rhdm-retail-webshop-demo/-/archive/master/rhdm-retail-webshop-demo-master.zip)

2. Add products to installs directory, see installs/README for details and links.

3. Run 'init.sh' or 'init.bat' file. 'init.bat' must be run with Administrative privileges, follow displayed instructions to start demo.


Demo run
--------
To experience the powers of centralized decision management, follow these steps (outlined in [this workshop
lab](https://bpmworkshop.gitlab.io/rhdm/lab10.html#/)):

1. After logging in to decision central (http://localhost:8080/decision-central  u:erics / p: redhatdm1!), open the project and deploy from the assets view.

2. Open the retail web shop front-end (http://localhost:8080/webstore-demo) and shop by adding items to basket and watch the shipping price climb as each threshold is
	 passed (total value in shopping cart passes 25, 50, 75, and 100).

3. Now open the 'Shipping Rules' decision table and adjust the field 'Shipping Total' for row 2, from 99.99 to 4.99 (as the postal
   service has discovered their pricing mistake on tier two).

4. Save the decision table and return to asset view to build and deploy the project again.

5. Clear the retail web shop cart and start shopping again... notice the threshold pass 25 and the shipping costs are now 12.99.
  

Notes
-----
Access retail web shop front-end to start shopping at: http://localhost:8080/webstore-demo

Kie Servier access configured (u: kieserver / p:kieserver1!). Enjoy installed and configured Red Hat Decision Manager.


Supporting Articles
-------------------
- [Beginners guide - Building a retail web shop workshop update](https://www.schabell.org/2020/07/beginners-guide-building-a-retail-web-shop-workshop-update.html)

- [Beginners Guide - Building a retail web shop workshop](https://www.schabell.org/2020/03/beginners-guide-building-retail-web-shop-workshop.html)


Released versions
-----------------
See the tagged releases for the following versions of the product:

- v1.3 - JBoss EAP 7.3 and Red Hat Decision Manager 7.8 locally with retail webshop installed.

- v1.2 - JBoss EAP 7.2 and Red Hat Decision Manager 7.7 locally with retail webshop installed.

- v1.1 - JBoss EAP 7.2 and Red Hat Decision Manager 7.6 locally with retail webshop installed.

- v1.0 - JBoss EAP 7.2 and Red Hat Decision Manager 7.5 locally with retail webshop installed.

![Web Shop](https://gitlab.com/bpmworkshop/rhdm-retail-webshop-demo/raw/master/docs/demo-images/rhdm-component-frontend.png)

![Web Shop Use](https://gitlab.com/bpmworkshop/rhdm-retail-webshop-demo/raw/master/docs/demo-images/rhdm-component-frontend-use.png)

![RHDM Login](https://gitlab.com/bpmworkshop/rhdm-retail-webshop-demo/raw/master/docs/demo-images/rhdm-login.png)

![RHDM Decision Central](https://gitlab.com/bpmworkshop/rhdm-retail-webshop-demo/raw/master/docs/demo-images/rhdm-decision-central.png)
